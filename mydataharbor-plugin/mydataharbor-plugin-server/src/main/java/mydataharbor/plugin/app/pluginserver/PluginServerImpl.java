/**
 *                                  Apache License
 *                            Version 2.0, January 2004
 *                         http://www.apache.org/licenses/
 *
 *    TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION
 *
 *    1. Definitions.
 *
 *       "License" shall mean the terms and conditions for use, reproduction,
 *       and distribution as defined by Sections 1 through 9 of this document.
 *
 *       "Licensor" shall mean the copyright owner or entity authorized by
 *       the copyright owner that is granting the License.
 *
 *       "Legal Entity" shall mean the union of the acting entity and all
 *       other entities that control, are controlled by, or are under common
 *       control with that entity. For the purposes of this definition,
 *       "control" means (i) the power, direct or indirect, to cause the
 *       direction or management of such entity, whether by contract or
 *       otherwise, or (ii) ownership of fifty percent (50%) or more of the
 *       outstanding shares, or (iii) beneficial ownership of such entity.
 *
 *       "You" (or "Your") shall mean an individual or Legal Entity
 *       exercising permissions granted by this License.
 *
 *       "Source" form shall mean the preferred form for making modifications,
 *       including but not limited to software source code, documentation
 *       source, and configuration files.
 *
 *       "Object" form shall mean any form resulting from mechanical
 *       transformation or translation of a Source form, including but
 *       not limited to compiled object code, generated documentation,
 *       and conversions to other media types.
 *
 *       "Work" shall mean the work of authorship, whether in Source or
 *       Object form, made available under the License, as indicated by a
 *       copyright notice that is included in or attached to the work
 *       (an example is provided in the Appendix below).
 *
 *       "Derivative Works" shall mean any work, whether in Source or Object
 *       form, that is based on (or derived from) the Work and for which the
 *       editorial revisions, annotations, elaborations, or other modifications
 *       represent, as a whole, an original work of authorship. For the purposes
 *       of this License, Derivative Works shall not include works that remain
 *       separable from, or merely link (or bind by name) to the interfaces of,
 *       the Work and Derivative Works thereof.
 *
 *       "Contribution" shall mean any work of authorship, including
 *       the original version of the Work and any modifications or additions
 *       to that Work or Derivative Works thereof, that is intentionally
 *       submitted to Licensor for inclusion in the Work by the copyright owner
 *       or by an individual or Legal Entity authorized to submit on behalf of
 *       the copyright owner. For the purposes of this definition, "submitted"
 *       means any form of electronic, verbal, or written communication sent
 *       to the Licensor or its representatives, including but not limited to
 *       communication on electronic mailing lists, source code control systems,
 *       and issue tracking systems that are managed by, or on behalf of, the
 *       Licensor for the purpose of discussing and improving the Work, but
 *       excluding communication that is conspicuously marked or otherwise
 *       designated in writing by the copyright owner as "Not a Contribution."
 *
 *       "Contributor" shall mean Licensor and any individual or Legal Entity
 *       on behalf of whom a Contribution has been received by Licensor and
 *       subsequently incorporated within the Work.
 *
 *    2. Grant of Copyright License. Subject to the terms and conditions of
 *       this License, each Contributor hereby grants to You a perpetual,
 *       worldwide, non-exclusive, no-charge, royalty-free, irrevocable
 *       copyright license to reproduce, prepare Derivative Works of,
 *       publicly display, publicly perform, sublicense, and distribute the
 *       Work and such Derivative Works in Source or Object form.
 *
 *    3. Grant of Patent License. Subject to the terms and conditions of
 *       this License, each Contributor hereby grants to You a perpetual,
 *       worldwide, non-exclusive, no-charge, royalty-free, irrevocable
 *       (except as stated in this section) patent license to make, have made,
 *       use, offer to sell, sell, import, and otherwise transfer the Work,
 *       where such license applies only to those patent claims licensable
 *       by such Contributor that are necessarily infringed by their
 *       Contribution(s) alone or by combination of their Contribution(s)
 *       with the Work to which such Contribution(s) was submitted. If You
 *       institute patent litigation against any entity (including a
 *       cross-claim or counterclaim in a lawsuit) alleging that the Work
 *       or a Contribution incorporated within the Work constitutes direct
 *       or contributory patent infringement, then any patent licenses
 *       granted to You under this License for that Work shall terminate
 *       as of the date such litigation is filed.
 *
 *    4. Redistribution. You may reproduce and distribute copies of the
 *       Work or Derivative Works thereof in any medium, with or without
 *       modifications, and in Source or Object form, provided that You
 *       meet the following conditions:
 *
 *       (a) You must give any other recipients of the Work or
 *           Derivative Works a copy of this License; and
 *
 *       (b) You must cause any modified files to carry prominent notices
 *           stating that You changed the files; and
 *
 *       (c) You must retain, in the Source form of any Derivative Works
 *           that You distribute, all copyright, patent, trademark, and
 *           attribution notices from the Source form of the Work,
 *           excluding those notices that do not pertain to any part of
 *           the Derivative Works; and
 *
 *       (d) If the Work includes a "NOTICE" text file as part of its
 *           distribution, then any Derivative Works that You distribute must
 *           include a readable copy of the attribution notices contained
 *           within such NOTICE file, excluding those notices that do not
 *           pertain to any part of the Derivative Works, in at least one
 *           of the following places: within a NOTICE text file distributed
 *           as part of the Derivative Works; within the Source form or
 *           documentation, if provided along with the Derivative Works; or,
 *           within a display generated by the Derivative Works, if and
 *           wherever such third-party notices normally appear. The contents
 *           of the NOTICE file are for informational purposes only and
 *           do not modify the License. You may add Your own attribution
 *           notices within Derivative Works that You distribute, alongside
 *           or as an addendum to the NOTICE text from the Work, provided
 *           that such additional attribution notices cannot be construed
 *           as modifying the License.
 *
 *       You may add Your own copyright statement to Your modifications and
 *       may provide additional or different license terms and conditions
 *       for use, reproduction, or distribution of Your modifications, or
 *       for any such Derivative Works as a whole, provided Your use,
 *       reproduction, and distribution of the Work otherwise complies with
 *       the conditions stated in this License.
 *
 *    5. Submission of Contributions. Unless You explicitly state otherwise,
 *       any Contribution intentionally submitted for inclusion in the Work
 *       by You to the Licensor shall be under the terms and conditions of
 *       this License, without any additional terms or conditions.
 *       Notwithstanding the above, nothing herein shall supersede or modify
 *       the terms of any separate license agreement you may have executed
 *       with Licensor regarding such Contributions.
 *
 *    6. Trademarks. This License does not grant permission to use the trade
 *       names, trademarks, service marks, or product names of the Licensor,
 *       except as required for reasonable and customary use in describing the
 *       origin of the Work and reproducing the content of the NOTICE file.
 *
 *    7. Disclaimer of Warranty. Unless required by applicable law or
 *       agreed to in writing, Licensor provides the Work (and each
 *       Contributor provides its Contributions) on an "AS IS" BASIS,
 *       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *       implied, including, without limitation, any warranties or conditions
 *       of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A
 *       PARTICULAR PURPOSE. You are solely responsible for determining the
 *       appropriateness of using or redistributing the Work and assume any
 *       risks associated with Your exercise of permissions under this License.
 *
 *    8. Limitation of Liability. In no event and under no legal theory,
 *       whether in tort (including negligence), contract, or otherwise,
 *       unless required by applicable law (such as deliberate and grossly
 *       negligent acts) or agreed to in writing, shall any Contributor be
 *       liable to You for damages, including any direct, indirect, special,
 *       incidental, or consequential damages of any character arising as a
 *       result of this License or out of the use or inability to use the
 *       Work (including but not limited to damages for loss of goodwill,
 *       work stoppage, computer failure or malfunction, or any and all
 *       other commercial damages or losses), even if such Contributor
 *       has been advised of the possibility of such damages.
 *
 *    9. Accepting Warranty or Additional Liability. While redistributing
 *       the Work or Derivative Works thereof, You may choose to offer,
 *       and charge a fee for, acceptance of support, warranty, indemnity,
 *       or other liability obligations and/or rights consistent with this
 *       License. However, in accepting such obligations, You may act only
 *       on Your own behalf and on Your sole responsibility, not on behalf
 *       of any other Contributor, and only if You agree to indemnify,
 *       defend, and hold each Contributor harmless for any liability
 *       incurred by, or claims asserted against, such Contributor by reason
 *       of your accepting any such warranty or additional liability.
 *
 *    END OF TERMS AND CONDITIONS
 *
 *    APPENDIX: How to apply the Apache License to your work.
 *
 *       To apply the Apache License to your work, attach the following
 *       boilerplate notice, with the fields enclosed by brackets "[]"
 *       replaced with your own identifying information. (Don't include
 *       the brackets!)  The text should be enclosed in the appropriate
 *       comment syntax for the file format. We also recommend that a
 *       file or class name and description of purpose be included on the
 *       same "printed page" as the copyright notice for easier
 *       identification within third-party archives.
 *
 *    Copyright 2021 徐浪 1053618636@qq.com
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package mydataharbor.plugin.app.pluginserver;

import lombok.extern.slf4j.Slf4j;
import mydataharbor.constant.Constant;
import mydataharbor.plugin.api.*;
import mydataharbor.plugin.api.exception.PluginServerCreateException;
import mydataharbor.plugin.api.node.NodeInfo;
import mydataharbor.plugin.api.plugin.PluginServerConfig;
import mydataharbor.plugin.app.listener.GroupChangeListener;
import mydataharbor.plugin.app.listener.GroupNodeChildrenChangeListener;
import mydataharbor.plugin.app.listener.NodeLeaderLatchListener;
import mydataharbor.plugin.app.plugin.PluginInfoManager;
import mydataharbor.plugin.app.rebalance.CommonRebalance;
import mydataharbor.plugin.app.rpc.RemoteManagerImpl;
import mydataharbor.plugin.app.task.TaskManager;
import mydataharbor.rpc.server.IRpcServer;
import mydataharbor.rpc.server.NettyRpcRpcServer;
import mydataharbor.rpc.util.JsonUtil;
import mydataharbor.util.VersionUtil;
import org.apache.commons.lang3.StringUtils;
import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.cache.NodeCache;
import org.apache.curator.framework.recipes.cache.PathChildrenCache;
import org.apache.curator.framework.recipes.leader.LeaderLatch;
import org.apache.curator.framework.recipes.leader.LeaderLatchListener;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.data.Stat;
import org.pf4j.DefaultPluginManager;
import org.pf4j.PluginManager;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.nio.file.Paths;
import java.util.List;

/**
 * @auth xulang
 * @Date 2021/6/11
 **/
@Slf4j
public class PluginServerImpl implements IPluginServer {

  private Yaml yaml;

  /**
   * 服务配置
   */
  private PluginServerConfig pluginServerConfig;

  /**
   * 节点信息
   */
  private NodeInfo nodeInfo;

  /**
   * zk客户端
   */
  private CuratorFramework client;

  /**
   * 是否停止
   */
  private volatile boolean stop;

  private Thread awaitThread;

  private IPluginRemoteManager pluginRemoteManager;

  private PluginManager pluginManager;

  private IRpcServer rpcServer;

  private IPluginInfoManager pluginInfoManager;

  private ITaskManager taskManager;

  private IRebalance rebalance;

  public PluginServerImpl(String runJarPath) {
    File file = new File(runJarPath);
    if (file.isFile()) {
      runJarPath = file.getParent() + "/";
    }
    this.yaml = new Yaml();
    try {
      this.pluginServerConfig = yaml.loadAs(new FileInputStream(runJarPath + "/" + Constant.CONFIG_FILE_PATH + "/" + Constant.CONFIG_FILE_NAME), PluginServerConfig.class);
      resolveSystemConfig(pluginServerConfig);
      this.nodeInfo = new NodeInfo(pluginServerConfig);
      this.nodeInfo.setVersion(VersionUtil.getVersion());
    } catch (FileNotFoundException e) {
      log.error("pluginserver创建失败：无法读取配置文件", e);
      throw new PluginServerCreateException("pluginserver创建失败：无法读取配置文件", e);
    }
    nodeInfo.setRunJarPath(runJarPath);
    String path = nodeInfo.getRunJarPath() + Constant.PLUGIN_PATH;
    log.info("plugins path: {}", path);
    pluginManager = new DefaultPluginManager(Paths.get(new File(path).getAbsolutePath()));
    this.pluginInfoManager = new PluginInfoManager(this);
    this.rebalance = new CommonRebalance();
  }

  /**
   * 解析系统配置
   *
   * @param pluginServerConfig
   */
  private void resolveSystemConfig(PluginServerConfig pluginServerConfig) {
    String group = System.getProperty("group");
    if (group != null) {
      pluginServerConfig.setGroup(group);
    }
    String nodeName = System.getProperty("nodeName");
    if (nodeName != null) {
      pluginServerConfig.setNodeName(nodeName);
    }
    String ip = System.getProperty("ip");
    if (ip != null) {
      pluginServerConfig.setIp(ip);
    }
    if (System.getProperty("port") != null) {
      Integer port = Integer.valueOf(System.getProperty("port"));
      if (port != null) {
        pluginServerConfig.setPort(port);
      }
    }
    String zk = System.getProperty("zk");
    if (zk != null) {
      pluginServerConfig.setZk(JsonUtil.jsonToObject(zk, List.class));
    }
    String pluginRepository = System.getProperty("pluginRepository");
    if (pluginRepository != null) {
      pluginServerConfig.setPluginRepository(pluginRepository);
    }
  }

  @Override
  public void start() {
    initPluginFramework();
    initRpcServer();
    initRemoteManager();
    if (pluginServerConfig.getZk() != null && pluginServerConfig.getZk().size() > 0) {
      RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 30);
      String zkAddress = StringUtils.join(pluginServerConfig.getZk(), ",");
      client = CuratorFrameworkFactory.newClient(zkAddress, retryPolicy);
      client.start();
      this.taskManager = new TaskManager(pluginInfoManager, client, this);
      initZkNodeInfo();
      initWatcher();
    }
  }

  private void initRpcServer() {
    this.rpcServer = new NettyRpcRpcServer(nodeInfo.getIp() + ":" + nodeInfo.getPort());
    try {
      rpcServer.start();
    } catch (InterruptedException e) {
      throw new PluginServerCreateException("启动rpc server异常！", e);
    }
  }


  private void initWatcher() {
    String lockPath = Constant.NODE_PREFIX + "/" + Constant.LEADER + "/" + nodeInfo.getGroup();
    LeaderLatch leaderLatch = new LeaderLatch(client, lockPath);
    LeaderLatchListener listener = new NodeLeaderLatchListener(nodeInfo, client);
    leaderLatch.addListener(listener);
    try {
      Stat stat = client.checkExists().forPath(lockPath);
      if (stat == null) {
        client.create().creatingParentsIfNeeded().forPath(lockPath);
      }
      leaderLatch.start();
    } catch (Exception e) {
      throw new PluginServerCreateException("启动leader监听器失败！", e);
    }

    String groupPath = Constant.NODE_PREFIX + "/" + Constant.NODE_NAME + "/" + nodeInfo.getGroup();
    //监听group节点
    NodeCache nodeCache = new NodeCache(client, groupPath);
    try {
      nodeCache.start();
      GroupChangeListener groupChangeListener = new GroupChangeListener(nodeCache, pluginRemoteManager, taskManager, this, client);
      nodeCache.getListenable().addListener(groupChangeListener);
    } catch (Exception e) {
      throw new PluginServerCreateException("启动父节点监听异常！", e);
    }
    GroupNodeChildrenChangeListener groupNodeChildrenChangeListener = new GroupNodeChildrenChangeListener(nodeInfo, rebalance);
    //监听group node子节点
    try {
      PathChildrenCache pathChildrenCache = new PathChildrenCache(client, groupPath, true);
      pathChildrenCache.getListenable().addListener(groupNodeChildrenChangeListener);
      pathChildrenCache.start();
    } catch (Exception e) {
      throw new PluginServerCreateException("启动同组内节点变化监听器失败！", e);
    }

  }


  private void initPluginFramework() {
    pluginManager.loadPlugins();
    pluginManager.startPlugins();
  }

  private void initRemoteManager() {
    this.pluginRemoteManager = new RemoteManagerImpl(this, pluginInfoManager, taskManager);
    rpcServer.addService(IPluginRemoteManager.class.getName(), "1.0", pluginRemoteManager);
  }

  @Override
  public void stop() {
    //各种close
    client.close();
    stop = true;
    pluginManager.stopPlugins();
    rpcServer.stop();
    log.info("jvm退出，关闭实例");
  }

  @Override
  public void startDaemonAwaitThread() {
    this.awaitThread = new AwaitThread("awaitThread");
    awaitThread.setContextClassLoader(this.getClass().getClassLoader());
    awaitThread.setDaemon(false);
    awaitThread.start();
  }

  @Override
  public PluginServerConfig getPluginServerConfig() {
    return pluginServerConfig;
  }

  @Override
  public PluginManager getPluginManager() {
    return pluginManager;
  }

  @Override
  public NodeInfo getNodeInfo() {
    return nodeInfo;
  }

  public void initZkNodeInfo() {
    String path = generateRegistPath();
    try {
      Stat exist = client.checkExists().forPath(path);
      while (exist != null) {
        log.warn("节点:{}已经存在！更换节点名称重试...", path);
        nodeInfo.rename();
        path = generateRegistPath();
        exist = client.checkExists().forPath(path);
      }
    } catch (Exception e) {
      throw new PluginServerCreateException("初始化zk失败！", e);
    }
    String nodeInfoJson = JsonUtil.objectToJson(nodeInfo);
    try {
      client.create().creatingParentsIfNeeded().withMode(CreateMode.EPHEMERAL).forPath(path, nodeInfoJson.getBytes());
    } catch (Exception e) {
      log.error("向zk注册节点发生异常", e);
      throw new PluginServerCreateException("向zk注册节点发生异常");
    }
  }


  /**
   * 生成zk注册地址
   *
   * @return
   */
  public String generateRegistPath() {
    return Constant.NODE_PREFIX + "/" + Constant.NODE_NAME + "/" + nodeInfo.getGroup() + "/" + nodeInfo.getNodeName();
  }

  class AwaitThread extends Thread {

    public AwaitThread(String name) {
      super(name);
    }

    public void run() {
      while (!stop) {
        try {
          Thread.sleep(10000);
        } catch (InterruptedException ex) {
          // continue and check the flag
        }
      }
    }
  }


}