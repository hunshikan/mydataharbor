/**
 *                                  Apache License
 *                            Version 2.0, January 2004
 *                         http://www.apache.org/licenses/
 *
 *    TERMS AND CONDITIONS FOR USE, REPRODUCTION, AND DISTRIBUTION
 *
 *    1. Definitions.
 *
 *       "License" shall mean the terms and conditions for use, reproduction,
 *       and distribution as defined by Sections 1 through 9 of this document.
 *
 *       "Licensor" shall mean the copyright owner or entity authorized by
 *       the copyright owner that is granting the License.
 *
 *       "Legal Entity" shall mean the union of the acting entity and all
 *       other entities that control, are controlled by, or are under common
 *       control with that entity. For the purposes of this definition,
 *       "control" means (i) the power, direct or indirect, to cause the
 *       direction or management of such entity, whether by contract or
 *       otherwise, or (ii) ownership of fifty percent (50%) or more of the
 *       outstanding shares, or (iii) beneficial ownership of such entity.
 *
 *       "You" (or "Your") shall mean an individual or Legal Entity
 *       exercising permissions granted by this License.
 *
 *       "Source" form shall mean the preferred form for making modifications,
 *       including but not limited to software source code, documentation
 *       source, and configuration files.
 *
 *       "Object" form shall mean any form resulting from mechanical
 *       transformation or translation of a Source form, including but
 *       not limited to compiled object code, generated documentation,
 *       and conversions to other media types.
 *
 *       "Work" shall mean the work of authorship, whether in Source or
 *       Object form, made available under the License, as indicated by a
 *       copyright notice that is included in or attached to the work
 *       (an example is provided in the Appendix below).
 *
 *       "Derivative Works" shall mean any work, whether in Source or Object
 *       form, that is based on (or derived from) the Work and for which the
 *       editorial revisions, annotations, elaborations, or other modifications
 *       represent, as a whole, an original work of authorship. For the purposes
 *       of this License, Derivative Works shall not include works that remain
 *       separable from, or merely link (or bind by name) to the interfaces of,
 *       the Work and Derivative Works thereof.
 *
 *       "Contribution" shall mean any work of authorship, including
 *       the original version of the Work and any modifications or additions
 *       to that Work or Derivative Works thereof, that is intentionally
 *       submitted to Licensor for inclusion in the Work by the copyright owner
 *       or by an individual or Legal Entity authorized to submit on behalf of
 *       the copyright owner. For the purposes of this definition, "submitted"
 *       means any form of electronic, verbal, or written communication sent
 *       to the Licensor or its representatives, including but not limited to
 *       communication on electronic mailing lists, source code control systems,
 *       and issue tracking systems that are managed by, or on behalf of, the
 *       Licensor for the purpose of discussing and improving the Work, but
 *       excluding communication that is conspicuously marked or otherwise
 *       designated in writing by the copyright owner as "Not a Contribution."
 *
 *       "Contributor" shall mean Licensor and any individual or Legal Entity
 *       on behalf of whom a Contribution has been received by Licensor and
 *       subsequently incorporated within the Work.
 *
 *    2. Grant of Copyright License. Subject to the terms and conditions of
 *       this License, each Contributor hereby grants to You a perpetual,
 *       worldwide, non-exclusive, no-charge, royalty-free, irrevocable
 *       copyright license to reproduce, prepare Derivative Works of,
 *       publicly display, publicly perform, sublicense, and distribute the
 *       Work and such Derivative Works in Source or Object form.
 *
 *    3. Grant of Patent License. Subject to the terms and conditions of
 *       this License, each Contributor hereby grants to You a perpetual,
 *       worldwide, non-exclusive, no-charge, royalty-free, irrevocable
 *       (except as stated in this section) patent license to make, have made,
 *       use, offer to sell, sell, import, and otherwise transfer the Work,
 *       where such license applies only to those patent claims licensable
 *       by such Contributor that are necessarily infringed by their
 *       Contribution(s) alone or by combination of their Contribution(s)
 *       with the Work to which such Contribution(s) was submitted. If You
 *       institute patent litigation against any entity (including a
 *       cross-claim or counterclaim in a lawsuit) alleging that the Work
 *       or a Contribution incorporated within the Work constitutes direct
 *       or contributory patent infringement, then any patent licenses
 *       granted to You under this License for that Work shall terminate
 *       as of the date such litigation is filed.
 *
 *    4. Redistribution. You may reproduce and distribute copies of the
 *       Work or Derivative Works thereof in any medium, with or without
 *       modifications, and in Source or Object form, provided that You
 *       meet the following conditions:
 *
 *       (a) You must give any other recipients of the Work or
 *           Derivative Works a copy of this License; and
 *
 *       (b) You must cause any modified files to carry prominent notices
 *           stating that You changed the files; and
 *
 *       (c) You must retain, in the Source form of any Derivative Works
 *           that You distribute, all copyright, patent, trademark, and
 *           attribution notices from the Source form of the Work,
 *           excluding those notices that do not pertain to any part of
 *           the Derivative Works; and
 *
 *       (d) If the Work includes a "NOTICE" text file as part of its
 *           distribution, then any Derivative Works that You distribute must
 *           include a readable copy of the attribution notices contained
 *           within such NOTICE file, excluding those notices that do not
 *           pertain to any part of the Derivative Works, in at least one
 *           of the following places: within a NOTICE text file distributed
 *           as part of the Derivative Works; within the Source form or
 *           documentation, if provided along with the Derivative Works; or,
 *           within a display generated by the Derivative Works, if and
 *           wherever such third-party notices normally appear. The contents
 *           of the NOTICE file are for informational purposes only and
 *           do not modify the License. You may add Your own attribution
 *           notices within Derivative Works that You distribute, alongside
 *           or as an addendum to the NOTICE text from the Work, provided
 *           that such additional attribution notices cannot be construed
 *           as modifying the License.
 *
 *       You may add Your own copyright statement to Your modifications and
 *       may provide additional or different license terms and conditions
 *       for use, reproduction, or distribution of Your modifications, or
 *       for any such Derivative Works as a whole, provided Your use,
 *       reproduction, and distribution of the Work otherwise complies with
 *       the conditions stated in this License.
 *
 *    5. Submission of Contributions. Unless You explicitly state otherwise,
 *       any Contribution intentionally submitted for inclusion in the Work
 *       by You to the Licensor shall be under the terms and conditions of
 *       this License, without any additional terms or conditions.
 *       Notwithstanding the above, nothing herein shall supersede or modify
 *       the terms of any separate license agreement you may have executed
 *       with Licensor regarding such Contributions.
 *
 *    6. Trademarks. This License does not grant permission to use the trade
 *       names, trademarks, service marks, or product names of the Licensor,
 *       except as required for reasonable and customary use in describing the
 *       origin of the Work and reproducing the content of the NOTICE file.
 *
 *    7. Disclaimer of Warranty. Unless required by applicable law or
 *       agreed to in writing, Licensor provides the Work (and each
 *       Contributor provides its Contributions) on an "AS IS" BASIS,
 *       WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
 *       implied, including, without limitation, any warranties or conditions
 *       of TITLE, NON-INFRINGEMENT, MERCHANTABILITY, or FITNESS FOR A
 *       PARTICULAR PURPOSE. You are solely responsible for determining the
 *       appropriateness of using or redistributing the Work and assume any
 *       risks associated with Your exercise of permissions under this License.
 *
 *    8. Limitation of Liability. In no event and under no legal theory,
 *       whether in tort (including negligence), contract, or otherwise,
 *       unless required by applicable law (such as deliberate and grossly
 *       negligent acts) or agreed to in writing, shall any Contributor be
 *       liable to You for damages, including any direct, indirect, special,
 *       incidental, or consequential damages of any character arising as a
 *       result of this License or out of the use or inability to use the
 *       Work (including but not limited to damages for loss of goodwill,
 *       work stoppage, computer failure or malfunction, or any and all
 *       other commercial damages or losses), even if such Contributor
 *       has been advised of the possibility of such damages.
 *
 *    9. Accepting Warranty or Additional Liability. While redistributing
 *       the Work or Derivative Works thereof, You may choose to offer,
 *       and charge a fee for, acceptance of support, warranty, indemnity,
 *       or other liability obligations and/or rights consistent with this
 *       License. However, in accepting such obligations, You may act only
 *       on Your own behalf and on Your sole responsibility, not on behalf
 *       of any other Contributor, and only if You agree to indemnify,
 *       defend, and hold each Contributor harmless for any liability
 *       incurred by, or claims asserted against, such Contributor by reason
 *       of your accepting any such warranty or additional liability.
 *
 *    END OF TERMS AND CONDITIONS
 *
 *    APPENDIX: How to apply the Apache License to your work.
 *
 *       To apply the Apache License to your work, attach the following
 *       boilerplate notice, with the fields enclosed by brackets "[]"
 *       replaced with your own identifying information. (Don't include
 *       the brackets!)  The text should be enclosed in the appropriate
 *       comment syntax for the file format. We also recommend that a
 *       file or class name and description of purpose be included on the
 *       same "printed page" as the copyright notice for easier
 *       identification within third-party archives.
 *
 *    Copyright 2021 徐浪 1053618636@qq.com
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package mydataharbor.web.service.impl;

import mydataharbor.constant.Constant;
import mydataharbor.plugin.api.IPluginRemoteManager;
import mydataharbor.plugin.api.exception.PluginLoadException;
import mydataharbor.plugin.api.exception.UninstallPluginException;
import mydataharbor.plugin.api.group.GroupInfo;
import mydataharbor.plugin.api.node.NodeInfo;
import mydataharbor.plugin.api.plugin.PluginInfo;
import mydataharbor.web.mapper.IPluginMapper;
import mydataharbor.web.service.IGroupChangeAction;
import mydataharbor.web.service.INodeService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.curator.RetryPolicy;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.recipes.cache.TreeCache;
import org.apache.curator.retry.ExponentialBackoffRetry;
import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.data.Stat;
import org.pf4j.PluginDescriptor;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import mydataharbor.rpc.client.RpcClient;
import mydataharbor.rpc.util.JsonUtil;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @auth xulang
 * @Date 2021/6/23
 **/
@Component
@Slf4j
public class NodeService implements INodeService, InitializingBean {

  @Value("${zk}")
  private List<String> zk;

  /**
   * zk客户端
   */
  private CuratorFramework client;

  private Map<String, IPluginRemoteManager> rpcPluginServerMap = new ConcurrentHashMap<>();

  private volatile Map<String, NodeInfo> nodeInfoCache = new ConcurrentHashMap<>();

  private volatile Map<String, GroupInfo> groupInfoCache = new ConcurrentHashMap<>();

  @Autowired
  private IPluginMapper pluginMapper;

  @Override
  public Map<String, List<NodeInfo>> lisNode() {
    Map<String, List<NodeInfo>> nodeInfoMap = new ConcurrentHashMap<>();
    Set<Map.Entry<String, NodeInfo>> entries = nodeInfoCache.entrySet();
    for (Map.Entry<String, NodeInfo> entry : entries) {
      List<NodeInfo> nodeInfos = nodeInfoMap.get(entry.getValue().getGroup());
      if (nodeInfos == null) {
        nodeInfos = new ArrayList<>();
        nodeInfoMap.put(entry.getValue().getGroup(), nodeInfos);
      }
      nodeInfos.add(entry.getValue());
    }
    return nodeInfoMap;
  }

  @Override
  public Map<String, GroupInfo> listGroupInfo() {
    return groupInfoCache;
  }

  @Override
  public void groupTouch(String groupName, IGroupChangeAction groupChangeAction) {
    //查询group信息
    String groupPath = Constant.NODE_PREFIX + "/" + Constant.NODE_NAME + "/" + groupName;
    try {
      while (true) {
        try {
          Stat stat = new Stat();
          byte[] data = client.getData().storingStatIn(stat).forPath(groupPath);
          if (groupChangeAction != null) {
            GroupInfo groupInfo = JsonUtil.deserialize(data, GroupInfo.class);
            groupChangeAction.action(groupInfo);
            client.setData().withVersion(stat.getVersion()).forPath(groupPath, JsonUtil.serialize(groupInfo));
          } else {
            client.setData().withVersion(stat.getVersion()).forPath(groupPath, data);
          }
          break;
        } catch (KeeperException.BadVersionException e) {
          log.warn("乐观锁重试", e);
        }
      }
    } catch (Exception e) {
      throw new RuntimeException("zk操作失败！", e);
    }
  }

  /**
   * 通过nodeName 获得rpc代理
   *
   * @param nodeName
   * @return
   */
  public IPluginRemoteManager getRpcPluginServerByNodeName(String nodeName) {
    NodeInfo nodeInfo = nodeInfoCache.get(nodeName);
    if (nodeInfo == null) {
      throw new RuntimeException("没有该机器注册！");
    }
    IPluginRemoteManager rpcPluginServer = rpcPluginServerMap.get(nodeName);
    if (rpcPluginServer == null) {
      synchronized (NodeService.class) {
        rpcPluginServer = rpcPluginServerMap.get(nodeName);
        if (rpcPluginServer == null) {
          try {
            rpcPluginServer = RpcClient.createService(IPluginRemoteManager.class, nodeInfo.getIp(), nodeInfo.getPort() + "");
          } catch (Exception e) {
            throw new RuntimeException("创建rpc服务出错！", e);
          }
        }
        rpcPluginServerMap.put(nodeName, rpcPluginServer);
      }
    }
    return rpcPluginServer;
  }

  @Override
  public List<PluginInfo> getPluginInfoByGroupName(String groupName) {
    GroupInfo groupInfo = groupInfoCache.get(groupName);
    if (groupInfo == null) {
      throw new RuntimeException("该分组不存在！");
    }
    NodeInfo[] nodeInfos = groupInfo.getNodeInfos().toArray(new NodeInfo[groupInfo.getNodeInfos().size()]);
    List<PluginInfo> installedPlugins = groupInfo.getInstalledPlugins();
    if (nodeInfos == null || nodeInfos.length == 0) {
      return installedPlugins;
    }
    //每次请求随机选取
    int index = RandomUtils.nextInt(0, nodeInfos.length);
    for (PluginInfo installedPlugin : installedPlugins) {
      IPluginRemoteManager pluginRemoteManager = getRpcPluginServerByNodeName(nodeInfos[index].getNodeName());
      PluginInfo pluginInfo = pluginRemoteManager.getPluginInfoByPluginId(installedPlugin.getPluginId());
      if (pluginInfo == null) {
        //插件安装失败
        continue;
      }
      installedPlugin.setDataSinkCreatorInfos(pluginInfo.getDataSinkCreatorInfos());
    }
    return installedPlugins;
  }

  /**
   * 获取groupinfo
   *
   * @return
   */
  public Map<String, GroupInfo> fetchGroupInfo() {
    Map<String, GroupInfo> groupInfos = new ConcurrentHashMap<>();
    try {
      String path = Constant.NODE_PREFIX + "/" + Constant.NODE_NAME;
      Stat stat = client.checkExists().forPath(path);
      if (stat == null) {
        log.warn("没有机器注册！");
        return Collections.emptyMap();
      }
      List<String> groupChildren = client.getChildren().forPath(path);
      for (String groupChild : groupChildren) {
        String groupPath = path + "/" + groupChild;
        byte[] bytes = client.getData().forPath(groupPath);
        if (bytes != null && bytes.length > 0) {
          GroupInfo groupInfo = JsonUtil.deserialize(bytes, GroupInfo.class);
          groupInfos.put(groupInfo.getGroupName(), groupInfo);
        } else {
          GroupInfo groupInfo = new GroupInfo();
          groupInfo.setGroupName(groupChild);
          groupInfos.put(groupChild, groupInfo);
        }
      }
    } catch (Exception e) {
      throw new RuntimeException("通过zk获取节点列表失败！", e);
    }
    return groupInfos;
  }

  /**
   * 获取节点信息
   *
   * @return
   */
  public Map<String, NodeInfo> fetchNodeInfo() {
    Map<String, NodeInfo> nodeInfos = new ConcurrentHashMap<>();
    try {
      String path = Constant.NODE_PREFIX + "/" + Constant.NODE_NAME;
      Stat stat = client.checkExists().forPath(path);
      if (stat == null) {
        log.warn("没有机器注册！");
        return Collections.emptyMap();
      }
      List<String> groupChildren = client.getChildren().forPath(path);
      for (String groupChild : groupChildren) {
        String groupPath = path + "/" + groupChild;
        List<String> nodeChildren = client.getChildren().forPath(groupPath);
        for (String nodeChild : nodeChildren) {
          String childPath = groupPath + "/" + nodeChild;
          byte[] bytes = client.getData().forPath(childPath);
          NodeInfo nodeInfo = JsonUtil.deserialize(bytes, NodeInfo.class);
          nodeInfos.put(nodeInfo.getNodeName(), nodeInfo);
          GroupInfo groupInfo = groupInfoCache.get(nodeInfo.getGroup());
          if (groupInfo != null) {
            groupInfo.getNodeInfos().add(nodeInfo);
          }
        }
      }
    } catch (Exception e) {
      throw new RuntimeException("通过zk获取节点列表失败！", e);
    }
    return nodeInfos;
  }

  @Override
  public void afterPropertiesSet() throws Exception {
    RetryPolicy retryPolicy = new ExponentialBackoffRetry(1000, 30);
    String zkAddress = StringUtils.join(zk, ",");
    client = CuratorFrameworkFactory.newClient(zkAddress, retryPolicy);
    client.start();
    this.groupInfoCache = fetchGroupInfo();
    this.nodeInfoCache = fetchNodeInfo();
    String path = Constant.NODE_PREFIX + "/" + Constant.NODE_NAME;
    TreeCache treeCache = new TreeCache(client, path);
    treeCache.getListenable().addListener((client, event) -> {
      this.groupInfoCache = fetchGroupInfo();
      this.nodeInfoCache = fetchNodeInfo();
    });
    treeCache.start();
  }

  /**
   * 安装插件
   */
  @Override
  public PluginInfo installPluginByRpcUpload(String fileName, PluginDescriptor pluginDescriptor, byte[] body, String groupName) {
    //查询group信息
    String groupPath = Constant.NODE_PREFIX + "/" + Constant.NODE_NAME + "/" + groupName;
    try {
      List<String> groupNodes = client.getChildren().forPath(groupPath);
      if (groupNodes.size() == 0) {
        //还没有节点启动
        throw new PluginLoadException("还没有节点启动");
      }
      //并行安装
      groupNodes.parallelStream().forEach(nodeName -> {
        try {
          String pluginId = getRpcPluginServerByNodeName(nodeName).loadPluginByRpc(fileName, body);
          getRpcPluginServerByNodeName(nodeName).startPlugin(pluginId);
        } catch (Exception e) {
          log.error("插件安装中有某些机器失败！组名:{},机器号：{}", groupName, nodeName);
        }
      });
    } catch (Exception e) {
      throw new PluginLoadException("error:" + e.getMessage(), e);
    }
    GroupInfo groupInfo;
    PluginInfo pluginInfo;
    try {
      while (true) {
        groupInfo = new GroupInfo();
        pluginInfo = new PluginInfo();
        try {
          Stat stat = new Stat();
          byte[] bytes = client.getData().storingStatIn(stat).forPath(groupPath);
          if (bytes != null && bytes.length > 0) {
            groupInfo = JsonUtil.deserialize(bytes, GroupInfo.class);
          } else {
            groupInfo.setGroupName(groupName);
          }
          pluginInfo.fillByPluginDescriptor(pluginDescriptor);
          //判断插件是否已经安装
          List<PluginInfo> installedPlugins = groupInfo.getInstalledPlugins();
          for (PluginInfo installedPlugin : installedPlugins) {
            if (installedPlugin.getPluginId().equals(pluginInfo.getPluginId())) {
              throw new RuntimeException(pluginInfo.getPluginId() + "该插件已经安装！安装信息：" + installedPlugin.toString());
            }
          }
          groupInfo.getInstalledPlugins().add(pluginInfo);
          String groupInfoJson = JsonUtil.objectToJson(groupInfo);
          client.setData().withVersion(stat.getVersion()).forPath(groupPath, groupInfoJson.getBytes());
          break;
        } catch (KeeperException.BadVersionException e) {
          log.warn("乐观锁生效，重试...", e);
        }
      }
    } catch (Exception e) {
      log.error("error！");
      throw new PluginLoadException("error:" + e.getMessage(), e);
    }


    return pluginInfo;
  }

  @Override
  public PluginInfo installPluginByReporsitory(String pluginId, String version, PluginDescriptor pluginDescriptor, String groupName) {
    //查询group信息
    String groupPath = Constant.NODE_PREFIX + "/" + Constant.NODE_NAME + "/" + groupName;
    GroupInfo groupInfo;
    PluginInfo pluginInfo;
    try {
      while (true) {
        groupInfo = new GroupInfo();
        pluginInfo = new PluginInfo();
        try {
          Stat stat = new Stat();
          byte[] bytes = client.getData().storingStatIn(stat).forPath(groupPath);
          if (bytes != null && bytes.length > 0) {
            groupInfo = JsonUtil.deserialize(bytes, GroupInfo.class);
          } else {
            groupInfo.setGroupName(groupName);
          }
          pluginInfo.fillByPluginDescriptor(pluginDescriptor);
          //判断插件是否已经安装
          List<PluginInfo> installedPlugins = groupInfo.getInstalledPlugins();
          for (PluginInfo installedPlugin : installedPlugins) {
            if (installedPlugin.getPluginId().equals(pluginInfo.getPluginId())) {
              throw new RuntimeException(pluginInfo.getPluginId() + "该插件已经安装！安装信息：" + installedPlugin.toString());
            }
          }
          groupInfo.getInstalledPlugins().add(pluginInfo);
          String groupInfoJson = JsonUtil.objectToJson(groupInfo);
          client.setData().withVersion(stat.getVersion()).forPath(groupPath, groupInfoJson.getBytes());
          break;
        } catch (KeeperException.BadVersionException e) {
          log.warn("乐观锁生效", e);
        }
      }
    } catch (Exception e) {
      log.error("error！");
      throw new PluginLoadException("error:" + e.getMessage(), e);
    }
    return pluginInfo;
  }

  public IPluginRemoteManager getRpcPluginServer(String nodeName) {
    return getRpcPluginServerByNodeName(nodeName);
  }

  @Override
  public boolean uninstallPlugin(String pluginId, String groupName) {
    //查询group信息
    String groupPath = Constant.NODE_PREFIX + "/" + Constant.NODE_NAME + "/" + groupName;

    try {
      while (true) {
        try {
          Stat stat = new Stat();
          byte[] bytes = client.getData().storingStatIn(stat).forPath(groupPath);
          GroupInfo groupInfo = null;
          if (bytes != null && bytes.length > 0) {
            groupInfo = JsonUtil.deserialize(bytes, GroupInfo.class);
          }
          if (groupInfo == null) {
            throw new UninstallPluginException("没有该groupName:" + groupName);
          }
          Iterator<PluginInfo> iterator = groupInfo.getInstalledPlugins().iterator();
          while (iterator.hasNext()) {
            PluginInfo pluginInfo = iterator.next();
            if (pluginInfo.getPluginId().equals(pluginId)) {
              iterator.remove();
            }
          }
          String groupInfoJson = JsonUtil.objectToJson(groupInfo);
          client.setData().withVersion(stat.getVersion()).forPath(groupPath, groupInfoJson.getBytes());
          break;
        } catch (KeeperException.BadVersionException e) {
          log.warn("乐观锁生效,重试...", e);
        }
      }
    } catch (Exception e) {
      log.error("error！");
      throw new PluginLoadException("error:" + e.getMessage(), e);
    }
    return true;
  }

  @Override
  public CuratorFramework getClient() {
    return client;
  }
}